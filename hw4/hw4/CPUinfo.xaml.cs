﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CPUinfo : ContentPage
	{
		public CPUinfo ()
		{
			InitializeComponent ();
		}

        //user is redirected to the url of the PC part when the text is clicked
        private void Button_Clicked(object sender, EventArgs e)
        {
           Device.OpenUri(new Uri("https://www.amd.com/en/products/cpu/amd-ryzen-7-2700x"));
        }

        //overrides the OnAppearing method
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(3000);
            BackgroundColor = Color.WhiteSmoke;
            img.Source = "cpu.jpg";
            img.HeightRequest = 180;
            img.WidthRequest = 180;
            txt.TextColor = Color.Salmon;
        }
    }
}