﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Memoryinfo2 : ContentPage
	{
		public Memoryinfo2 ()
		{
			InitializeComponent ();
		}

        //user is redirected to the url of the PC part when the text is clicked
        private void Button_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.corsair.com/us/en/Categories/Products/Memory/Vengeance-PRO-RGB-Black/p/CMW16GX4M2D3000C16"));
        }

        //overrides the OnAppearing method
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(3000);
            BackgroundColor = Color.WhiteSmoke;
            img.Source = "mem1.png";
            txt.TextColor = Color.Salmon;
        }
    }
}