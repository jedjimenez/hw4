﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw4
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
        private ObservableCollection<GroupedByParts> list { get; set; }

        public Page1 ()
		{
			InitializeComponent ();
            getParts();
           
        }

        //function that adds the parts information by groups. reference can be found in the "sources.txt"
        private void getParts()
        {
            list = new ObservableCollection<GroupedByParts>();

            var CPU = new GroupedByParts() { Title = "CPU" };
            CPU.Add(new Parts() { id = 1, name = "AMD Ryzen 7 2700X", description = "3.7 GHz 8-Core Processor", img = "ryz1.jpg" });
            CPU.Add(new Parts() { id = 2, name = "Intel Core i7-6700K", description = "4 GHz Quad-Core Processor", img = "intel.jpg" });
            list.Add(CPU);

            var Memory = new GroupedByParts() { Title = "Memory" };
            Memory.Add(new Parts() { id = 3, name = "Corsair Vengeance LPX 16 GB", description = "(2 x 8 GB) DDR4-3000 Memory", img = "ven.jpg" });
            Memory.Add(new Parts() { id = 4, name = "Corsair Vengeance RGB Pro 16 GB", description = "(2 x 8 GB) DDR4-3200 Memory", img = "ven1.jpg" });
            list.Add(Memory);

            var Mobo = new GroupedByParts() { Title = "Motherboard"};
            Mobo.Add(new Parts() { id = 5, name = "MSI Z170A GAMING M5", description = "ATX LGA1151 Motherboard", img = "lga.jpg" });
            Mobo.Add(new Parts() { id = 6, name = "Asus STRIX B350-F GAMING", description = "ATX AM4 Motherboard", img = "amd.jpg" });
            list.Add(Mobo);

            var GPU = new GroupedByParts() { Title = "Video Card" };
            GPU.Add(new Parts() { id = 7, name = "Asus GeForce RTX 2080 Ti", description = "11 GB ROG Strix Gaming OC Video Card", img = "rtx.jpg" } );
            GPU.Add(new Parts() { id = 8, name = "XFX Radeon RX 5700", description = "8 GB DD Ultra Video Card", img = "xfx.jpg" });
            list.Add(GPU);

            PCParts.ItemsSource = list;
        }

        //context menu that changes the background color based on user option
        public async void OnEdit(object sender, EventArgs e)
        {
            /*var mi = ((MenuItem)sender);
            var part = (Parts)mi.CommandParameter;*/

            string color = await DisplayActionSheet("Choose a background color", "Cancel", null, "Honeydew", "Misty Rose", "Light Cyan");
            

            if (color == "Honeydew")
            {
                PCParts.BackgroundColor = Color.Honeydew;
            }
            else if (color == "Misty Rose")
            {
                PCParts.BackgroundColor = Color.MistyRose;

            }
            else if (color == "Light Cyan")
            {
                PCParts.BackgroundColor = Color.LightCyan;
            }
            else if (color == "Cancel")
            {
                PCParts.BackgroundColor = Color.Beige;
            }
        }


        //context menu that deletes the selected item by the user. implementation of this function for deleting an item in a group is found in the "sources.txt"
        public void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var deletePart = (Parts)mi.CommandParameter;

            for (int i = 0; i < list.Count; i++)
            {
                GroupedByParts parts = list[i];
                for (int j = 0; j < parts.Count; j++)
                {
                    if (parts[j] == deletePart)
                    {
                        list[i].RemoveAt(j);
                    }
                }
            }
  
        }

        //when a user selects an item, it shows a page of information about the PC part
       async private void PCParts_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var select = (Parts)e.Item;

            switch (select.id)
            {
                case 1:
                    await Navigation.PushAsync(new CPUinfo());
                    break;
                case 2:
                    await Navigation.PushAsync(new CPUinfo2());
                    break;
                case 3:
                    await Navigation.PushAsync(new Memoryinfo());
                    break;
                case 4:
                    await Navigation.PushAsync(new Memoryinfo2());
                    break;
                case 5:
                    await Navigation.PushAsync(new Moboinfo());
                    break;
                case 6:
                    await Navigation.PushAsync(new Moboinfo2());
                    break;
                case 7:
                    await Navigation.PushAsync(new GPUinfo());
                    break;
                case 8:
                    await Navigation.PushAsync(new GPUinfo2());
                    break;

            }
            ((ListView)sender).SelectedItem = null;
        }

        //function that handles the refreshing
        private void PCParts_Refreshing(object sender, EventArgs e)
        {
            getParts();
            PCParts.IsRefreshing = false;
        }
    }
}