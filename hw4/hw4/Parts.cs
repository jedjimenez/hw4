﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace hw4
{
    public class Parts
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string img { get; set; }
    }

    public class GroupedByParts : ObservableCollection<Parts>
    {
        public string Title { get; set; }
        
    }
}
