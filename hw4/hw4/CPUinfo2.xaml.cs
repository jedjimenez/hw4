﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CPUinfo2 : ContentPage
	{
		public CPUinfo2 ()
		{
			InitializeComponent ();
		}

        //user is redirected to the url of the PC part when the text is clicked
        private void Button_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://ark.intel.com/content/www/us/en/ark/products/88195/intel-core-i7-6700k-processor-8m-cache-up-to-4-20-ghz.html"));
        }

        //overrides the OnAppearing method
        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(3000);
            BackgroundColor = Color.WhiteSmoke;
            img.Source = "cpu1.jpg";
            txt.TextColor = Color.Salmon;
        }
    }
}